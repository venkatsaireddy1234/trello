import axios from "axios";


export const fetchBoardsData=()=>{
  const url = "https://api.trello.com/1/members/me/boards?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45"
    return axios.get(url)
}

export const addNewBoards = (boardName) => {
    const getBoardUrl= `https://api.trello.com/1/boards/?name=${boardName}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
    if (boardName) {
      return axios.post(getBoardUrl)
    }
  };


export const fetchLists=(boardId)=>{
  const getListsUrl = `https://api.trello.com/1/boards/${boardId}/lists?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.get(getListsUrl)
}

export const addNewList = (boardId,listName)=>{
  const addNewListUrl = `https://api.trello.com/1/boards/${boardId}/lists?name=${listName}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  if(listName){
    return axios.post(addNewListUrl)
  }
}

export const deleteList = (listId)=>{
  const deleteListUrl = `https://api.trello.com/1/lists/${listId}/closed?value=true&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.put(deleteListUrl)
} 


export const getCardOnBoards= (listId)=>{
  const getCardsUrl = `https://api.trello.com/1/lists/${listId}/cards?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.get(getCardsUrl)
}

export const createNewCard = (cardTitle,listId,)=>{
  const newCard = `https://api.trello.com/1/cards?name=${cardTitle}&idList=${listId}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  if(cardTitle){
    return axios.post(newCard)
  }
}


export const deleteCard = (cardId)=>{
  const cardUrl = `https://api.trello.com/1/cards/${cardId}?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.delete(cardUrl)
}

export const getCheckLists = (cardId)=>{
  const getCheckListsUrl= `https://api.trello.com/1/cards/${cardId}/checklists?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.get(getCheckListsUrl);
}



export const addCheckList = (checkListTitle,cardId)=>{
  const checkListUrl = `https://api.trello.com/1/checklists?name=${checkListTitle}&idCard=${cardId}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  if(checkListTitle){
    return axios.post(checkListUrl)
  }
}

export const deleteCheckList = (checkListId)=>{
  const deleteUrl = `https://api.trello.com/1/checklists/${checkListId}?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.delete(deleteUrl)
}

export const getCheckItems = (checkListId) => {
  const checkItemsUrl = `https://api.trello.com/1/checklists/${checkListId}/checkItems?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.get(checkItemsUrl)
};


export const updateCheckItem = (cardId, checkItemId, checkedStatus) => {
  const upgradeCheckItemUrl = `https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=${checkedStatus}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.put(upgradeCheckItemUrl)
    
};

export const createCheckItem = (checkListId,checkItemTitle) => {
  const createCheckItemUrl = `https://api.trello.com/1/checklists/${checkListId}/checkItems?name=${checkItemTitle}&key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  if (checkItemTitle) {
    return axios.post(createCheckItemUrl)
  }
};

export const deleteCheckItem = (checkListId,checkItemId) => {
  const deleteCheckItemUrl = `https://api.trello.com/1/checklists/${checkListId}/checkItems/${checkItemId}?key=a914971bf73f3dd63bececf5028a5472&token=5dd24dd4a85a41e956a67d06f1f18bac07ed445e60aaf7c90c1e6a5c39ec8a45`
  return axios.delete(deleteCheckItemUrl)
}