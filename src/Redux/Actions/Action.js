

export const DISPLAY_ALL_BOARDS = "boards/displayAllBoards"

export const displayAllBoards=(boardsData)=>{
    return{
        type:DISPLAY_ALL_BOARDS,
        payload:boardsData
    }
}

export const ADD_NEW_BOARDS = "boards/addBoards"
export const addBoards = (newBoard) => {
    return {
      type: ADD_NEW_BOARDS,
      payload: newBoard,
    };
  };

export const DISPLAY_ALL_LISTS = 'lists/displayAllLists'
export const displayAllLists=(listData)=>{
  return {
    type:DISPLAY_ALL_LISTS,
    payload:listData
  }
}

export const ADD_NEW_LIST = 'lists/addNewList'
export const addingNewList = (newList)=>{
  return {
    type:ADD_NEW_LIST,
    payload:newList
  }
} 

export const DELETE_LIST = 'lists/deleteList'
export const deletingList = (listId)=>{
  return {
    type:DELETE_LIST,
    payload:listId
  }
}

export const DISPLAY_ALL_CARDS = 'lists/displayAllCards'
export const displayAllCards = (listId,cardsData)=>{
  return {
    type:DISPLAY_ALL_CARDS,
    id:listId,
    payload:cardsData
  }
}

export const ADD_NEW_CARD = 'lists/createNewCard'
export const addNewCard = (listId,cardTitle)=>{
  return{
    type:ADD_NEW_CARD,
    id:listId,
    payload:cardTitle
  }
} 

export const DELETE_CARD = 'lists/deleteListCard'
export const deleteListCard = (listId,cardId)=>{
  return {
    type:DELETE_CARD,
    id:listId,
    payload:cardId
  }
}

export const DISPLAY_CHECKLISTS = 'cards/checklists'
export const displayCheckLists = (checkListData)=>{
  return {
    type:DISPLAY_CHECKLISTS,
    payload:checkListData
  }
}

export const ADD_CHECKLIST = 'cards/addNewCheckLists'
export const addCheckLists = (data)=>{
  return {
    type:ADD_CHECKLIST,
    payload:data
  }
} 

export const DELETE_CHECKLIST = 'cards/removeCheckList'
export const removeCheckList = (cardId,checkListId)=>{
  return {
    type:DELETE_CHECKLIST,
    id:cardId,
    payload:checkListId
  }
}

