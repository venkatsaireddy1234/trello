import * as actions from "../Actions/Action"
import { combineReducers } from "redux";

const boardsReducer =(boards = [], action)=>{
    switch(action.type){
        case actions.DISPLAY_ALL_BOARDS:
            return [...action.payload]
        case actions.ADD_NEW_BOARDS:
            return [...boards, action.payload];
        default:
            return boards;
    }

}


const listsReducer = (lists= [], action)=>{
    switch(action.type){
        case actions.DISPLAY_ALL_LISTS:
            return [...action.payload]
        case actions.ADD_NEW_LIST:
            return [...lists,action.payload]
        case actions.DELETE_LIST:
            return lists.filter((list)=>list.id !== action.payload)
        default:
            return lists;
    }
}


const cardsReducer = (state = {}, action)=>{
    switch(action.type){
        case actions.DISPLAY_ALL_CARDS:
            return {...state, [action.id]:action.payload};
        case actions.ADD_NEW_CARD:
            return {...state, [action.id]:[...state[action.id],action.payload]};
        case actions.DELETE_CARD:
            return {...state,[action.id]:state[action.id].filter(card=>card.id!==action.payload)}
        default:
            return state;
    }
}

const checkListReducer = (checkLists=[], action)=>{
    switch(action.type){
        case actions.DISPLAY_CHECKLISTS:
            return [...action.payload]
        case actions.ADD_CHECKLIST:  
            return [...checkLists,action.payload]
        case actions.DELETE_CHECKLIST:
            return checkLists.filter((checklist)=>checklist.id!==action.payload)
        default:
            return checkLists
    }
}




export default combineReducers({boardsReducer,listsReducer,cardsReducer,checkListReducer});