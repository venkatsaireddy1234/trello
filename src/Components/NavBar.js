import React, { Component } from "react";
import { Link } from "react-router-dom";

class NavBar extends Component {
  render() {
    return (
      <>
        <nav className="navbar col-12 d-flex">
          <Link className="navbar-home" to="/boards">
            Home
          </Link>
          <Link className="navbar-logo-container" to="/boards">
            <div className="navbar-logo"></div>
          </Link>
        </nav>
      </>
    );
  }
}

export default NavBar;
