import React, { Component } from "react";
import DeleteIcon from "../images/cross-logo.svg";
import PlusIcon from "../images/plus-icon.svg";
import { connect } from "react-redux";
import { createNewCard, deleteList, getCardOnBoards } from "../Redux/Apis";
import { deletingList, displayAllCards,addNewCard } from "../Redux/Actions/Action";
import DetailedEachCard from "./DetailedEachCard";
import "../App.css"

class EachList extends Component {
  state = {
    userInput: "",
    displayCardInput: false,
  };

  componentDidMount = () => {
    getCardOnBoards(this.props.eachList.id)
      .then((res) => res.data)
      .then((data) => {
        this.props.dispatch(displayAllCards(this.props.eachList.id, data));
      });
  };

  handleDelete = () => {
    deleteList(this.props.eachList.id)
      .then((res) => res.data.id)
      .then((id) => {
        this.props.dispatch(deletingList(id));
      });
  };

  handleNewCardInput = () => {
    this.setState({
      displayCardInput: true,
    });
  };

  handleNewCardTitle = (event) => {
    this.setState({
      userInput: event.target.value,
    });
  };

  handleAddCardOption = () => {
    this.setState({
      displayCardInput: false,
      userInput: "",
    });
  };


  addNewCardInList=(event)=>{
    event.preventDefault();
    createNewCard(this.state.userInput,this.props.eachList.id)
    .then((res)=>res.data)
    .then((data)=>{
      this.setState({
        displayCardInput:false,
        userInput:""
      },
      ()=> this.props.dispatch(addNewCard(this.props.eachList.id,data)))
    })
  }

  render() {
    return (
      <div className="m-2">
        <div className="board-list-container">
          <div className="d-flex justify-content-between">
            <h6>{this.props.eachList.name}</h6>
            <img
              onClick={this.handleDelete}
              className="board-list-close-icon align-self-start pointerCursor"
              src={DeleteIcon}
              alt="cross-icon.svg"
            />
          </div>
          {this.props.cards[this.props.eachList.id] ===undefined? null : this.props.cards[this.props.eachList.id].map((card) => (
            <DetailedEachCard
              key={card.id}
              cardDetails={card}
              listDetails={this.props.eachList}
            />
          ))}
          

          <div
            onClick={this.handleNewCardInput}
            className={
              this.state.displayCardInput
                ? "d-none"
                : "board-list-add-card-container d-flex align-items-center"
            }
          >
            <img
              className="board-list-add-card-icon"
              src={PlusIcon}
              alt="plus-icon.svg"
            />
            Add a card
          </div>
          <form
            onSubmit={this.addNewCardInList}
            className={this.state.displayCardInput ? "d-block" : "d-none"}
          >
            <textarea
              value={this.state.userInput}
              className="board-list-add-card-input col-12"
              placeholder="Enter a title for this card..."
              onChange={this.handleNewCardTitle}
            />
            <button className="btn-sm btn btn-primary">Add card</button>
            <button
              type="submit"
              onClick={this.handleAddCardOption}
              className="board-list-add-card-input-close-button btn-sm btn btn"
            >
              <img src={DeleteIcon} alt=""/>
            </button>
          </form>
        </div>
      </div>

    );
  }
}

const mapStateToProps = (state, x) => {
  return {
    cards: state.cardsReducer,
  };
};

export default connect(mapStateToProps)(EachList);
