import React, { Component } from "react";
import checklistIcon from "../images/checklist-icon.svg";
import DeleteIcon from "../images/cross-logo.svg";
import "../App.css";
import CheckItems from "./CheckItems"
import {getCheckItems,createCheckItem,deleteCheckItem} from "../Redux/Apis"
// import { connect } from "react-redux";

class DetailedEachCheckList extends Component {
  state = {
    checkItems: [],
    checkItemsInputDisplay: false,
    checkItemInput: "",
  };


  componentDidMount = () => {
    console.log(this.props);
    getCheckItems(this.props.checklistId)
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkItems: data,
        });
      });
  };

  handleCheckItemOpen = () => {
    this.setState({
      checkItemsInputDisplay: true,
    });
  };
  handleCheckItemClose = () => {
    this.setState({
      checkItemsInputDisplay: false,
    });
  };
  addNewCheckItem = (event) => {
    event.preventDefault();
    createCheckItem(
      this.props.checklistId,
      this.state.checkItemInput
    )
      .then((res) => res.data)
      .then((data) => {
        this.setState({
          checkItems: [...this.state.checkItems, data],
          checkItemsInputDisplay: false,
          checkItemInput: "",
        });
      });
  };

  handleCheckItemDelete = (checkItemId) => {
    deleteCheckItem(this.props.checklistId, checkItemId)
      .then((res) => res)
      .then((data) => {
        let newArray = this.state.checkItems.filter(
          (checkItem) => checkItem.id !== checkItemId
        );
        this.setState({
          checkItems: newArray,
        });
      });
  };
  handleCheckItemStatus = (checkItemDetails) => {
    let newArray = this.state.checkItems.map((checkItem) =>
      checkItem.id === checkItemDetails.id ? checkItemDetails : checkItem
    );
    this.setState({
      checkItems: newArray,
    });
  };
  handleCheckItemInput = (event) => {
    this.setState({
      checkItemInput: event.target.value,
    });
  };
  render() {
    return (
      <>
        <div className="trello-each-checklist-container mb-3">
          <div className="d-flex justify-content-between align-items-center">
            <div className="d-flex">
              <img
                className="trello-checklist-icon"
                src={checklistIcon}
                alt="checklist-icon.svg"
              />
              <p>{this.props.checkListName}</p>
            </div>
            <img
              className="pointerCursor"
              onClick={() =>
                this.props.deleteCheckList(this.props.checklistId)
              }
              src={DeleteIcon}
              alt="close-icon.svg"
            />
          </div>
          <div>
          {this.state.checkItems.map((checkItem) => (
            <CheckItems
              key={checkItem.id}
              cardDetails={this.props.cardDetails}
              checkItemsDetails={checkItem}
              checkItemStatus={this.handleCheckItemStatus}
              deleteCheckItem={this.handleCheckItemDelete}
            />
          ))}
        </div>
        <button
          onClick={this.handleCheckItemOpen}
          className={
            this.state.checkItemsInputDisplay
              ? "d-none"
              : "mt-2 text-light btn btn-info btn-sm"
          }
        >
          Add an item
        </button>

        <form
          onSubmit={this.addNewCheckItem}
          className={this.state.checkItemsInputDisplay ? "mt-2" : "d-none"}
        >
          <textarea
            onChange={this.handleCheckItemInput}
            value={this.state.checkItemInput}
            className="trello-checklist-add-checkitem-input form-control"
            type="text"
            placeholder="Add an item"
          />
          <div>
            <button className="btn btn-primary btn-sm">Add</button>

            <button
              onClick={this.handleCheckItemClose}
              className="trello-checklist-add-checkitem-close-button btn btn-secondary btn-sm"
            >
              Cancel
            </button>
          </div>
        </form>
        </div>
      </>
    );
  }
}

export default DetailedEachCheckList;
