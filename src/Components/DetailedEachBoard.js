import React, { Component } from 'react';
import { fetchLists,addNewList} from '../Redux/Apis';
import EachList from './EachList';
import { connect } from 'react-redux';
import { displayAllLists,addingNewList } from '../Redux/Actions/Action';
import PlusIcon from "../images/plus-icon.svg";
import crossLogo from "../images/cross-logo.svg"
import "../App.css"

class DetailedEachBoard extends Component {
    state = { 
        addListInput: false,
        userInput: "",
     } 

    handleNewListInput = () => {
    this.setState({
        addListInput: true,
    });
    };
    
    handleNewInputClose = () => {
    this.setState({
        addListInput: false,
        userInput: "",
    });
    };


    addNewList = (event) => {
        event.preventDefault();
        addNewList(this.props.match.params.boardId, this.state.userInput)
          .then((res) => res.data)
          .then((data) => {
            this.setState(
              {
                addListInput: false,
                userInput: "",
              },
              () => this.props.dispatch(addingNewList(data))
            );
          });
      };


      updateNewListTitle = (event) => {
        this.setState({
          userInput: event.target.value,
        });
      };



    componentDidMount = () => {
        fetchLists(this.props.match.params.boardId)
          .then((res) => res.data)
          .then((data) => {
            this.setState(() => this.props.dispatch(displayAllLists(data))
            );
          });
      };
    render() { 
        return (
        <>
        <div className="board-main-container d-flex">
        {this.props.lists.map((list) => (
          <EachList key={list.id} eachList={list} />
        ))}
        <div>
          <div
            className={
              this.state.addListInput ? "d-none" : "board-add-list-container"
            }
            onClick={this.handleNewListInput}
          >
            <img
              className="board-add-list-icon"
              src={PlusIcon}
              alt="plus-icon.svg"
            />
            Add another list
          </div>
          <form
            className={
              this.state.addListInput
                ? "board-add-list-input-container"
                : "d-none"
            }
            onSubmit={this.addNewList}
          >
            <input
              onChange={this.updateNewListTitle}
              value={this.state.userInput}
              className="col-12 mb-2 form-control"
              type="text"
              placeholder="Enter list title..."
            />
            <div>
              <button type="submit" className="btn btn-primary btn-sm">
                Add list
              </button>
              <button
                className="board-add-list-close-button btn btn btn-sm"
                onClick={this.handleNewInputClose}
              >
                <img src={crossLogo} alt=""/>
              </button>
            </div>
          </form>
        </div>
        </div>
        </>
        );
    }
}


const mapStateToProps=(state)=>{
    return {
        lists:state.listsReducer
    }
}
 
export default connect(mapStateToProps)(DetailedEachBoard);