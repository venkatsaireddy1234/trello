import React, { Component } from "react";
import { connect } from "react-redux";
import DeleteIcon from "../images/cross-logo.svg";
import "../App.css";
import { deleteCard, getCheckLists,addCheckList,deleteCheckList} from "../Redux/Apis";
import { addCheckLists, deleteListCard, displayCheckLists,removeCheckList } from "../Redux/Actions/Action";
import { Modal } from "react-bootstrap";
import DetailedEachCheckList from "./DetailedEachCheckList";

class DetailedEachCard extends Component {
  state = {
    checkLists: [],
    model: false,
    addCheckListInput: false,
    addCheckListButton: true,
    newCheckListTitle: "",
  };

  componentDidMount = () => {
    getCheckLists(this.props.cardDetails.id)
      .then((res) => res.data)
      .then((data) => { 
        console.log(data);
        this.setState(() => this.props.dispatch(displayCheckLists(data)));
      });
  };

  handleModelOpen = () => {
    this.setState({
      model: true,
    });
  };
  handleModelClose = () => {
    this.setState({
      model: false,
    });
  };

  handleAddCheckListInputOpen = () => {
    this.setState({
      addCheckListInput: true,
      addCheckListButton: false,
    });
  };
  handleAddCheckListInputClose = () => {
    this.setState({
      newCheckListTitle: "",
      addCheckListInput: false,
      addCheckListButton: true,
    });
  };

  handleDeleteCard = (listId, cardId) => {
    deleteCard(cardId)
    .then(() => {
      this.props.dispatch(deleteListCard(listId, cardId));
    });
  };

  addNewCheckList=(event)=>{
    event.preventDefault();
    addCheckList(this.state.newCheckListTitle,
      this.props.cardDetails.id)
      .then((res)=>res.data)
      .then((data)=>{
          console.log(data);
          this.setState({
            addCheckListInput: false,
            addCheckListButton: true,
            newCheckListTitle: "",
          },
            ()=>
          
          this.props.dispatch(addCheckLists(data)))
      })
  }
   
  handleNewCheckListTitle = (event) => {
    this.setState({
      newCheckListTitle: event.target.value,
    });
  };

  handleDeleteCheckList=(checkListId)=>{
    deleteCheckList(checkListId)
    .then((res)=>res)
    .then(()=>{
      this.setState(()=>
      this.props.dispatch(removeCheckList(this.cardId,checkListId)))
    })
  }





  render() {
    return (
      <>
        <div className="trello-each-card-container d-flex justify-content-between">
          <div onClick={this.handleModelOpen} className="pointerCursor col-11">
            <p className="m-0">{this.props.cardDetails.name}</p>
          </div>
          <img
            className="pointerCursor"
            onClick={() =>
              this.handleDeleteCard(
                this.props.listDetails.id,
                this.props.cardDetails.id
              )
            }
            src={DeleteIcon}
            alt="cross-icon.svg"
          />
        </div>

        <Modal show={this.state.model} onHide={this.handleModelClose}>
          <div className="trello-checklist-close-container d-flex justify-content-end">
            <img
              onClick={this.handleModelClose}
              className="trello-checklist-close-icon pointerCursor"
              src={DeleteIcon}
              alt="cross-icon.svg"
            />
          </div>
          <div className="trello-checklist-main-container">
            <div className="d-flex justify-content-between">
              <div>
                <h5>{this.props.cardDetails.name}</h5>
                <p className="trello-checklist-list-title">
                  {this.props.listDetails.name}
                </p>
              </div>
              <div>
                <button
                  onClick={this.handleAddCheckListInputOpen}
                  className={
                    this.state.addCheckListButton
                      ? "btn btn-primary btn-sm"
                      : "d-none"
                  }
                >
                  Add checkList
                </button>
                <button
                  onClick={this.handleAddCheckListInputClose}
                  className={
                    this.state.addCheckListButton
                      ? "d-none"
                      : "btn btn-secondary btn-sm"
                  }
                >
                  Close
                </button>
              </div>
            </div>
            <form
              onSubmit={this.addNewCheckList}
              className={
                this.state.addCheckListInput ? "d-flex mb-2" : "d-none"
              }
            >
              <input
                onChange={this.handleNewCheckListTitle}
                value={this.state.newCheckListTitle}
                type="text"
                className="form-control"
                placeholder="Enter a checklist title"
              />
              <button
                type="submit"
                className="trello-add-checklist-add-button btn btn-dark btn-sm"
              >
                Add
              </button>
            </form>
            <div>
              
              {this.props.checkList.map((checkList) => (
                <DetailedEachCheckList
                  checklistId={checkList.id}
                  checkListName = {checkList.name}
                  cardDetails={this.props.cardDetails}
                  deleteCheckList={this.handleDeleteCheckList}
                />
              ))}
            </div>
          </div>
        </Modal>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cards: state.cardsReducer,
    checkList:state.checkListReducer
  };
};
export default connect(mapStateToProps)(DetailedEachCard);
