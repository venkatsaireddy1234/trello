import React, { Component } from "react";
import { Link } from "react-router-dom";


class EachBoard extends Component {
  render() {  
    return (
      <Link  className="remove-text-decoration" to={`/boards/${this.props.eachBoard.id}`}> 
        <div className="trello-boards-board">{this.props.eachBoard.name}</div>
      </Link>
    );
  }
}

export default EachBoard;
