import React, { Component } from "react";
import { addNewBoards, fetchBoardsData } from "../Redux/Apis";
import { displayAllBoards, addBoards } from "../Redux/Actions/Action";
import EachBoard from "./EachBoard";
import { connect } from "react-redux";
import "./../App.css";
import { Modal } from "react-bootstrap";

class BoardComponent extends Component {
  state = {
    popUp: false,
    newBoardName: "",
  };
  componentDidMount = () => {
    fetchBoardsData().then((res) => {
      this.setState(() => this.props.dispatch(displayAllBoards(res.data)));
    });
  };

  handleNewBoardTitle = (event) => {
    this.setState({
      newBoardName: event.target.value,
    });
  };

  addingBoards = () => {
    this.props.boards.length === 10
      ? alert("Boards limit exceeded")
      : addNewBoards(this.state.newBoardName)
          .then((res) => res.data)
          .then((data) => {
            this.setState(
              {
                popUp: false,
                newBoardName: "",
              },
              () => this.props.dispatch(addBoards(data))
            );
          });
  };

  handleAddNewBoard = (event) => {
    event.preventDefault();
    this.addingBoards();
  };

  handleOnEnter = (event) => {
    if (event.key === "Enter") {
      this.addingBoards();
    }
  };

  handleAddBoardForm = () => {
    this.setState({
      popUp: true,
    });
  };

  handleModelPopUp = () => {
    this.setState({
      popUp: false,
      newBoardName: "",
    });
  };

  render() {
    console.log(this.props, "props hey buddy");
    return (
      <div>
        <div className=" yyy">
          {this.props.boards.map((board) => (
            <EachBoard key={board.id} eachBoard={board} />
          ))}
          <div
            onClick={this.handleAddBoardForm}
            className="trello-boards-create-board d-flex flex-column justify-content-center align-items-center"
          >
            <p>Create new board</p>
            <p className="trello-boards-count">
              Boards left: {10 - this.props.boards.length}
            </p>
          </div>
          <Modal show={this.state.popUp} onHide={this.handleModelPopUp}>
            <form
              className="trello-create-board-container"
              onSubmit={this.handleAddNewBoard}
            >
              <h4>Create board</h4>
              <hr />
              <label htmlFor="boardTitleInputElement" className="mt-3">
                Board title
              </label>
              <input
                onKeyDown={this.handleOnEnter}
                type="text"
                onChange={this.handleNewBoardTitle}
                value={this.state.newBoardName}
                id="boardTitleInputElement"
                className="col-12 form-control mt-2"
                placeholder="Enter a board title"
              />
              <div className="trello-create-board-buttons-container d-flex justify-content-end">
                <button
                  onClick={this.handleModelPopUp}
                  className="trello-create-board-close-button btn btn-secondary btn-sm"
                >
                  Close
                </button>
                <button type="submit" className="btn btn-light btn-sm col-4">
                  create
                </button>
              </div>
            </form>
          </Modal>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log(state, "map state");
  return {
    boards: state.boardsReducer,
  };
};
export default connect(mapStateToProps)(BoardComponent);
