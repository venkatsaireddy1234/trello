import React, { Component } from "react";
import { updateCheckItem } from "../Redux/Apis"
import SquareCloseIcon from "../images/square-close-icon.svg";

class CheckItem extends Component {
  handleCheckBox = (event) => {
    updateCheckItem(
      this.props.cardDetails.id,
      this.props.checkItemsDetails.id,
      event.target.checked
    )
      .then((res) => res.data)
      .then((data) => this.props.checkItemStatus(data));
  };
  render() {
    return (
      <div className="d-flex">
        <input
          onChange={this.handleCheckBox}
          id={this.props.checkItemsDetails.id}
          className="trello-each-checkitem-checkBox form-check-input"
          type="checkbox"
          checked={
            this.props.checkItemsDetails.state === "complete" ? true : false
          }
        />
        <label
          className={
            this.props.checkItemsDetails.state === "complete"
              ? "trello-each-checkitem-title trello-checkitem-checked"
              : "trello-each-checkitem-title"
          }
          htmlFor={this.props.checkItemsDetails.id}
        >
          {this.props.checkItemsDetails.name}
        </label>
        <img
          onClick={() =>
            this.props.deleteCheckItem(this.props.checkItemsDetails.id)
          }
          className="trello-checkitem-close-icon"
          src={SquareCloseIcon}
          alt="square-closeicon.svg"
        />
      </div>
    );
  }
}

export default CheckItem;
