import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import './App.css';
import BoardComponent from './Components/Boards';
import { Redirect } from "react-router-dom";
import NavBar from './Components/NavBar';
import 'bootstrap/dist/css/bootstrap.min.css';
import DetailedEachBoard from './Components/DetailedEachBoard';



export class  App extends Component {
  render(){
  return (
    <Router>
        <React.Fragment>
          <NavBar />
          <Switch>
            <Route exact path="/">
              <Redirect to="/boards" />
            </Route>
            <Route exact path="/boards" component={BoardComponent} />
            <Route exact path="/boards/:boardId" component={DetailedEachBoard} />
          </Switch>
        </React.Fragment>
      </Router>
  );
}
}

export default App;
